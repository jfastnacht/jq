FROM alpine:3.14

RUN set -ex; \
    apk update; \
    apk add jq bash git;

WORKDIR /

ENTRYPOINT [ "jq" ]
